#-------------------------------------------------
#
# Project created by QtCreator 2014-11-16T19:03:00
#
#-------------------------------------------------
include(General/General.pri)
include(Network/Network.pri)


QT       -= gui

TARGET = TaskModule
TEMPLATE = lib
CONFIG += staticlib

unix {
    target.path = /usr/lib
    INSTALLS += target
}
##

unix: LIBS += -L$$PWD/../../../../../../../../../usr/local/lib/ -lgf_complete

INCLUDEPATH += $$PWD/../../../../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../../../../usr/local/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../../../../../usr/local/lib/libgf_complete.a
##

unix: LIBS += -L$$PWD/../../../../../../../../../usr/local/lib/ -lJerasure

INCLUDEPATH += $$PWD/../../../../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../../../../usr/local/include
