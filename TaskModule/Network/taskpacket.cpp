#include "taskpacket.h"
#include <QMetaObject>
#include <QMetaProperty>

#include "tasktype.h"
#include "decodetaskdata.h"

TaskPacket::TaskPacket( qint32 fileID, qint8 OPcode, TaskData* taskData, QObject *parent)
    :QObject(parent)
{
    m_TaskData = NULL;
    setFileID(fileID);
    setOPcode(OPcode);
    setTaskData(taskData);
}

TaskPacket::TaskPacket( const TaskPacket& other ):QObject(0)
{
    setFileID(other.getFileID());
    setOPcode(other.getOPcode());
    TaskData* data;
    if(getOPcode() == Encoding){
        data = new TaskData(*other.getTaskData());
    }else{
        data = new DecodeTaskData(*dynamic_cast<DecodeTaskData*>(other.getTaskData()));
    }
    setTaskData(data);
}
TaskPacket& TaskPacket::operator=(const TaskPacket& other){
    setFileID(other.getFileID());
    setOPcode(other.getOPcode());
    TaskData* data;
    if(getOPcode() == Encoding){
        data = new TaskData(*other.getTaskData());
    }else{
        data = new DecodeTaskData(*dynamic_cast<DecodeTaskData*>(other.getTaskData()));
    }
    setTaskData(data);
    return *this;
}
TaskPacket::~TaskPacket(){
    if(m_TaskData) delete m_TaskData;
}

qint32    TaskPacket::getFileID()  const{
    return m_FileID;
}
qint8     TaskPacket::getOPcode()  const{
    return m_OPcode;
}
TaskData*  TaskPacket::getTaskData()const{
    return m_TaskData;
}

void TaskPacket::setFileID(qint32 fileID){
    m_FileID = fileID;
}
void TaskPacket::setOPcode(qint8 doneWorkNum){
    m_OPcode = doneWorkNum;
}
void TaskPacket::setTaskData(TaskData* taskData){
    m_TaskData = taskData;
}

QDataStream& operator<<(QDataStream& ds, const TaskPacket& obj){
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds << obj.metaObject()->property(i).read(&obj);
        }
    }
    switch((TaskType)obj.getOPcode()){
    case Encoding:
        ds << *obj.getTaskData();
        break;
    case Decoding:
        ds << *dynamic_cast<DecodeTaskData*>(obj.getTaskData());
        break;
    }
    return ds;
}
QDataStream& operator>>(QDataStream& ds, TaskPacket & obj){
    QVariant var;
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds >> var;
            obj.metaObject()->property(i).write(&obj, var);
        }
    }
    TaskData* taskData;
    DecodeTaskData* decodetaskdata;
    switch((TaskType)obj.getOPcode()){
    case Encoding:
        taskData = new TaskData();
        ds >> *taskData;
        break;
    case Decoding:
        decodetaskdata = new DecodeTaskData();
        ds >> *decodetaskdata;
        taskData = decodetaskdata;
        break;
    }

    obj.setTaskData(taskData);
    return ds;
}
