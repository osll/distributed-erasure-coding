#include "taskdata.h"
#include <QMetaObject>
#include <QMetaProperty>

TaskData::TaskData( QString dataPath, qint32 dataOffset,
                    QString codePath, qint32 codeOffset,
                    qint32  blockSize       , qint32 workNum )
{
    setDataPath(dataPath);
    setDataOffset(dataOffset);
    setCodePath(codePath);
    setCodeOffset(codeOffset);
    setBlockSize(blockSize);
    setWorkNum(workNum);
}

TaskData::TaskData( const TaskData& other )
{
    setDataPath     (other.getDataPath());
    setDataOffset   (other.getDataOffset());
    setCodePath     (other.getCodePath());
    setCodeOffset   (other.getCodeOffset());
    setBlockSize    (other.getBlockSize());
    setWorkNum      (other.getWorkNum());
}
TaskData& TaskData::operator=(const TaskData& other){
    setDataPath     (other.getDataPath());
    setDataOffset   (other.getDataOffset());
    setCodePath     (other.getCodePath());
    setCodeOffset   (other.getCodeOffset());
    setBlockSize    (other.getBlockSize());
    setWorkNum      (other.getWorkNum());
    return *this;
}

TaskData::~TaskData(){}


QString TaskData::getDataPath()const{
    return m_DataPath;
}
qint32  TaskData::getDataOffset()      const{
    return m_DataOffset;
}
QString TaskData::getCodePath()const{
    return m_CodePath;
}
qint32  TaskData::getCodeOffset()      const{
    return m_CodeOffset;
}
qint32  TaskData::getBlockSize()      const{
    return m_BlockSize;
}
qint32  TaskData::getWorkNum()         const{
    return m_WorkNum;
}

void TaskData::setDataPath (QString path){
    m_DataPath          = path;
}
void TaskData::setDataOffset(qint32 dataOffset){
    m_DataOffset        = dataOffset;
}
void TaskData::setCodePath (QString path){
    m_CodePath          = path;
}
void TaskData::setCodeOffset(qint32 codeOffset){
    m_CodeOffset        = codeOffset;
}
void TaskData::setBlockSize(qint32 blockSize){
    m_BlockSize     = blockSize;
}
void TaskData::setWorkNum   (qint32 workNum){
    m_WorkNum           = workNum;
}

QDataStream& operator<<(QDataStream& ds, const TaskData& obj){
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds << obj.metaObject()->property(i).read(&obj);

        }
    }
    return ds;
}
QDataStream& operator>>(QDataStream& ds, TaskData& obj){
    QVariant var;
    for(int i=0; i<obj.metaObject()->propertyCount(); ++i) {
        if(obj.metaObject()->property(i).isStored(&obj)) {
            ds >> var;
            obj.metaObject()->property(i).write(&obj, var);
        }
    }
    return ds;
}
