#include "decodetaskdata.h"

DecodeTaskData::DecodeTaskData( QString dataPath, qint32 dataOffset,
                                QString codePath, qint32 codeOffset,
                                const QList<qint32>& erasures,
                                qint32  blockSize       , qint32 workNum )
    :TaskData( dataPath ,  dataOffset,
               codePath ,  codeOffset,
               blockSize,  workNum )
{
    m_pErasures = new QList<qint32>(erasures);
    if(m_pErasures->length() == 0)m_pErasures->push_front(-1);
}

void DecodeTaskData::addErasure(int id){
    m_pErasures->push_front(id);
}
QList<qint32>* DecodeTaskData::getErasures() const{
    return m_pErasures;
}

DecodeTaskData::~DecodeTaskData(){
    delete m_pErasures;
}

QDataStream& operator<<(QDataStream & ds, const DecodeTaskData & obj){
    QList<qint32>* erasures = obj.getErasures();
    Q_ASSERT(erasures->size() > 0);
    ds << (qint32) (erasures->size() - 1);
    for(QList<qint32>::iterator it = erasures->begin(); *it != -1; ++it){
        ds << *it;
    }
    ds << dynamic_cast<const TaskData&>(obj);
    return ds;
}

QDataStream& operator>>(QDataStream & ds, DecodeTaskData & obj){
    QList<qint32>* erasures = obj.getErasures();
    Q_ASSERT(erasures->size() > 0);
    qint32 size, temp;
    ds >> size;
    for (qint32 i = 0 ; i < size ; ++i){
        ds >> temp;
        obj.addErasure(temp);
    }
    ds >> dynamic_cast<TaskData&>(obj);
    return ds;
}
