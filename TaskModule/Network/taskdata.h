#ifndef TASKDATA_H
#define TASKDATA_H

#include <QObject>
#include <QDataStream>

class TaskData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString m_DataPath       READ getDataPath        WRITE setDataPath      )
    Q_PROPERTY(qint32  m_DataOffset     READ getDataOffset      WRITE setDataOffset    )
    Q_PROPERTY(QString m_CodePath       READ getCodePath        WRITE setCodePath      )
    Q_PROPERTY(qint32  m_CodeOffset     READ getCodeOffset      WRITE setCodeOffset    )
    Q_PROPERTY(qint32  m_BlockSize      READ getBlockSize       WRITE setBlockSize     )
    Q_PROPERTY(qint32  m_WorkNum        READ getWorkNum         WRITE setWorkNum       )

private:
    QString m_DataPath;
    qint32  m_DataOffset;
    QString m_CodePath;
    qint32  m_CodeOffset;
    qint32  m_BlockSize;
    qint32  m_WorkNum;

public:
    explicit TaskData ( QString dataPath = tr(""), qint32 dataOffset = -1,
                        QString codePath = tr(""), qint32 codeOffset = -1,
                        qint32  blockSize = -1, qint32  workNum = -1 );
    TaskData( const TaskData& other );
    TaskData& operator=(const TaskData& other);

    QString getDataPath()    const;
    qint32  getDataOffset()   const;
    QString getCodePath()    const;
    qint32  getCodeOffset()   const;
    qint32  getBlockSize()const;
    qint32  getWorkNum()      const;

    void setDataPath       (QString);
    void setDataOffset      (qint32 );
    void setCodePath       (QString);
    void setCodeOffset      (qint32 );
    void setBlockSize       (qint32 );
    void setWorkNum         (qint32 );

    virtual ~TaskData();

};

QDataStream& operator<<(QDataStream &, const TaskData &);
QDataStream& operator>>(QDataStream &, TaskData &);

#endif // TASKDATA_H
