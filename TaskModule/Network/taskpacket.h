#ifndef TASKPACKET_H
#define TASKPACKET_H

#include <QObject>
#include <QDataStream>
#include "taskdata.h"

class TaskPacket : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint32 m_FileID  READ getFileID  WRITE setFileID)
    Q_PROPERTY(qint8  m_OPcode  READ getOPcode  WRITE setOPcode)

private:
    qint32    m_FileID;
    qint8     m_OPcode;
    TaskData* m_TaskData;

public:
    explicit TaskPacket( qint32 fileID = -1, qint8 OPcode = -1, TaskData* taskData = NULL, QObject *parent = 0 );
    TaskPacket( const TaskPacket& other );
    TaskPacket& operator=(const TaskPacket& other);
    qint32    getFileID()  const;
    qint8     getOPcode()  const;
    TaskData* getTaskData()const;
    void setFileID(qint32 FileID);
    void setOPcode(qint8 DoneWorkNum);
    void setTaskData(TaskData* taskdata);

    ~TaskPacket();

};

QDataStream& operator<<(QDataStream &, const TaskPacket &);
QDataStream& operator>>(QDataStream &, TaskPacket &);

#endif // TASK_H
