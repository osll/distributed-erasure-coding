#ifndef NODEREPLY_H
#define NODEREPLY_H

#include <QObject>
#include <QDataStream>

class NodeReply : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint32 m_FileID      READ getFileID     WRITE setFileID)
    Q_PROPERTY(qint32 m_DoneWorkNum READ getDoneWorkNum WRITE setDoneWorkNum)

private:
    qint32 m_FileID;
    qint32 m_DoneWorkNum;

public:
    explicit NodeReply( qint32 FileID = -1, qint32 DoneWorkNum = -1);
    NodeReply( const NodeReply& other );
    NodeReply& operator=(const NodeReply& other);
    qint32 getFileID()const;
    qint32 getDoneWorkNum()const;
    void setFileID(qint32 FileID);
    void setDoneWorkNum(qint32 DoneWorkNum);

};

QDataStream& operator<<(QDataStream &, const NodeReply &);
QDataStream& operator>>(QDataStream &, NodeReply &);

#endif // NODEREPLY_H
