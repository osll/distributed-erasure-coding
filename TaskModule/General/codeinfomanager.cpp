#include "codeinfomanager.h"
#include "reedsolomonvandermondeinfo.h"


CodeInfoManager * CodeInfoManager::p_instance = 0;
CodeInfoManagerDestroyer CodeInfoManager::destroyer;

CodeInfoManagerDestroyer::~CodeInfoManagerDestroyer() {
    delete p_instance;
}
void CodeInfoManagerDestroyer::initialize( CodeInfoManager* p ) {
    p_instance = p;
}

CodeInfoManager::CodeInfoManager()
{
    m_pCodeInfo =  NULL;
}

void CodeInfoManager::clear(){
    if(m_pCodeInfo != NULL) delete m_pCodeInfo;
}

CodeInfoManager* CodeInfoManager::getInstance() {
    if(!p_instance)     {
        p_instance = new CodeInfoManager();
        destroyer.initialize( p_instance);
    }
    return p_instance;
}


void CodeInfoManager::setRSVM(int K,int M,int W){
    CodeInfoManager* manager = getInstance();
    manager->clear();
    manager->m_pCodeInfo = new ReedSolomonVandermondeInfo(K,M,W);
}

qint32 CodeInfoManager::getK(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getK();
    else return -1;
}

qint32 CodeInfoManager::getM(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getM();
    else return -1;
}

qint32 CodeInfoManager::getW(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getW();
    else return -1;
}

qint32 CodeInfoManager::getBlockSize(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getBlockSize();
    else return -1;
}

qint32 CodeInfoManager::getDataSize(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getDataSize();
    else return -1;
}

qint32 CodeInfoManager::getCodeSize(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getCodeSize();
    else return -1;
}

int*   CodeInfoManager::getMatrix(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getMatrix();
    else return NULL;
}

MethodType CodeInfoManager::getMethodType(){
    CodeInfoManager* manager = getInstance();
    if (manager->m_pCodeInfo)return manager->m_pCodeInfo->getMethodType();
    else return Undefined;
}
