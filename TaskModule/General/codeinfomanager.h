#ifndef CODEINFOMANAGER_H
#define CODEINFOMANAGER_H

#include "codeinfo.h"

class CodeInfoManager;

class CodeInfoManagerDestroyer
{
private:
    CodeInfoManager* p_instance;
public:
    ~CodeInfoManagerDestroyer();
    void initialize( CodeInfoManager* p );
};

class CodeInfoManager
{
private:
    CodeInfo* m_pCodeInfo;

    static CodeInfoManager* p_instance;
    static CodeInfoManagerDestroyer destroyer;

protected:
    CodeInfoManager();
    CodeInfoManager( const CodeInfoManager& );
    CodeInfoManager& operator=( CodeInfoManager& );
   ~CodeInfoManager() { }
    friend class CodeInfoManagerDestroyer;

    void clear();
    static CodeInfoManager* getInstance();
public:
    static void setRSVM(int K,int M,int W);

    static qint32 getK();
    static qint32 getM();
    static qint32 getW();
    static qint32 getBlockSize();
    static qint32 getDataSize();
    static qint32 getCodeSize();
    static int*   getMatrix();
    static MethodType getMethodType();
};

#endif // CODEINFOMANAGER_H
