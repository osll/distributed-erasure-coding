#include "reedsolomonvandermondeinfo.h"
#include <jerasure/reed_sol.h>
ReedSolomonVandermondeInfo::ReedSolomonVandermondeInfo( qint32 K, qint32 M, qint32 W){
    m_K = K;
    m_M = M;
    m_W = W;
    m_BlockSize = m_W/8*sizeof(long);
    m_DataSize = m_K*m_BlockSize;
    m_CodeSize  = m_M*m_BlockSize;
    m_pMatrix = reed_sol_vandermonde_coding_matrix(m_K, m_M, m_W);
}

ReedSolomonVandermondeInfo::~ReedSolomonVandermondeInfo(){
    delete m_pMatrix;
}

qint32 ReedSolomonVandermondeInfo::getK(){
    return m_K;
}

qint32 ReedSolomonVandermondeInfo::getM(){
    return m_M;
}

qint32 ReedSolomonVandermondeInfo::getW(){
    return m_W;
}

qint32 ReedSolomonVandermondeInfo::getBlockSize(){
    return m_BlockSize;
}

qint32 ReedSolomonVandermondeInfo::getDataSize(){
    return m_DataSize;
}

qint32 ReedSolomonVandermondeInfo::getCodeSize(){
    return m_CodeSize;
}

MethodType ReedSolomonVandermondeInfo::getMethodType(){
    return ReedSolomonVandermondeMatrix;
}

int*   ReedSolomonVandermondeInfo::getMatrix(){
    return m_pMatrix;
}
