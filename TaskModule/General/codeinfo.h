#ifndef CODEINFO_H
#define CODEINFO_H

#include <QObject>
#include "methodtype.h"

class CodeInfo
{

public:
    virtual ~CodeInfo() = 0;

    virtual qint32 getK() = 0;
    virtual qint32 getM() = 0;
    virtual qint32 getW() = 0;
    virtual qint32 getBlockSize() = 0;
    virtual qint32 getDataSize() = 0;
    virtual qint32 getCodeSize() = 0;
    virtual int*   getMatrix() = 0;
    virtual MethodType getMethodType() = 0;
};

#endif // CODEINFO_H
