#ifndef REEDSOLOMONVANDERMONDEINFO_H
#define REEDSOLOMONVANDERMONDEINFO_H

#include "codeinfo.h"
#include "codeinfomanager.h"

class ReedSolomonVandermondeInfo : public CodeInfo
{
private:
    qint32 m_K;
    qint32 m_M;
    qint32 m_W;
    qint32 m_BlockSize;
    qint32 m_DataSize;
    qint32 m_CodeSize;
    int    *m_pMatrix;

protected:
    ReedSolomonVandermondeInfo(qint32 K,qint32 M,qint32 W);
    ~ReedSolomonVandermondeInfo();
    friend class CodeInfoManager;

public:
  qint32 getK();
  qint32 getM();
  qint32 getW();
  qint32 getBlockSize();
  qint32 getDataSize();
  qint32 getCodeSize();
  int*   getMatrix();
  MethodType getMethodType();


};

#endif // REEDSOLOMONVANDERMONDEINFO_H
