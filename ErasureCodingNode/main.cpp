#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "node.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList parametrs = a.arguments();
    if(parametrs.size() != 3){
        qDebug() << "Usage :";
        qDebug() << "\"ErasureCodingNode ipv4adpress port\"";
        a.exit(-1);
    }
    QString ipv4Adress = parametrs[1];
    int     port = parametrs[2].toInt();

    Node node(ipv4Adress,port,&a);

    return a.exec();
}
