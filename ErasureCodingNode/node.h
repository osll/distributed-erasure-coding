#ifndef NODE_H
#define NODE_H

#include <QObject>

#include <General/codeinfo.h>

#include "servermanager.h"
#include "taskmanager.h"
#include "console.h"

class Node : public QObject
{
    Q_OBJECT
private:
    ServerManager* m_pServerManager;
    TaskManager*   m_pTaskManager;

    Console*       m_pConsole;

public:
    Node(QString ipv4Adress, int port,QObject *parent = 0);
    ~Node();

signals:

public slots:
    void proceedeServerData(int numberOfThreads);
    void disconnectedFromHost();

};

#endif // NODE_H
