#include "rsvtaskfactory.h"

#include "encodersc.h"
#include "decodersc.h"

#include <Network/decodetaskdata.h>
RSVTaskFactory::RSVTaskFactory()
{

}

RSVTaskFactory::~RSVTaskFactory(){

}

RunnableTask* RSVTaskFactory::createEncodeTask(TaskPacket* packet){
    TaskData* data = packet->getTaskData();
    return new EncodeRSC(packet->getFileID(),
                        data->getDataPath(),data->getDataOffset(),
                        data->getCodePath(),data->getCodeOffset(),
                        data->getBlockSize(),data->getWorkNum());
}

RunnableTask* RSVTaskFactory::createDecodeTask(TaskPacket* packet){
    DecodeTaskData* data = dynamic_cast<DecodeTaskData*>(packet->getTaskData());
    return  new DecodeRSC(packet->getFileID() ,
                          data->getDataPath(),data->getDataOffset(),
                          data->getCodePath(),data->getCodeOffset(),
                          *data->getErasures(),
                          data->getBlockSize(),data->getWorkNum());
}
