#ifndef DECODERSC_H
#define DECODERSC_H

#include "runnabletask.h"

class DecodeRSC : public RunnableTask
{
    Q_OBJECT
private:
    qint32          m_FileID;
    QString         m_DataPath;
    qint32          m_DataOffset;
    QString         m_CodePath;
    qint32          m_CodeOffset;
    QList<qint32>   m_Erasures;
    qint32          m_BlockSize;
    qint32          m_WorkNum;

public:
    explicit DecodeRSC(qint32  fileID = -1      ,
                       QString dataPath = tr(""), qint32 dataOffset = -1,
                       QString codePath = tr(""), qint32 codeOffset = -1,
                       QList<qint32> erasures = QList<qint32>(),
                       qint32  blockSize = -1   , qint32 workNum = -1);
    ~DecodeRSC();

    void run();
signals:

public slots:

};

#endif // DECODERSC_H
