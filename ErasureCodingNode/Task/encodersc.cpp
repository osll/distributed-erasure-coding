#include "encodersc.h"

#include <QFile>
#include <QDebug>
#include <QVector>

#include <General/codeinfomanager.h>
#include <Network/nodereply.h>

#include <jerasure.h>
#include <jerasure/reed_sol.h>

EncodeRSC::EncodeRSC(qint32  fileID   ,
                     QString dataPath, qint32 dataOffset,
                     QString codePath, qint32 codeOffset,
                     qint32 blockSize , qint32 workNum)
{
    m_FileID = fileID;
    m_DataPath = dataPath;
    m_CodePath = codePath;
    m_DataOffset = dataOffset;
    m_CodeOffset = codeOffset;
    m_BlockSize = blockSize;
    m_WorkNum = workNum;
}

EncodeRSC::~EncodeRSC(){
}

void EncodeRSC::run(){
    QFile dataFile(m_DataPath);
    QFile codeFile(m_CodePath);

    qDebug() << m_DataOffset<< " "<< m_CodeOffset;

    if(!dataFile.open(QIODevice::ReadWrite))return;
    if(!codeFile.open(QIODevice::ReadWrite))return;

    QVector<char*> data(CodeInfoManager::getK(),NULL);
    QVector<char*> code(CodeInfoManager::getM(),NULL);

    for(int i = 0 ; i < CodeInfoManager::getK() ; ++i){
        data[i] =(char*) malloc(m_WorkNum * CodeInfoManager::getBlockSize());
        dataFile.seek( i*m_BlockSize +  m_DataOffset);
        dataFile.read(data[i],m_WorkNum * CodeInfoManager::getBlockSize());
    }
    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        code[i] =(char*) malloc(m_WorkNum * CodeInfoManager::getBlockSize());
    }

    qDebug() << "Кодирую :";
    jerasure_matrix_encode(CodeInfoManager::getK(), CodeInfoManager::getM(), CodeInfoManager::getW(),
                           CodeInfoManager::getMatrix(), data.data(), code.data(),
                           m_WorkNum * CodeInfoManager::getBlockSize());
    qDebug() << "Кодироваине завершено";

    if(!codeFile.exists())return;

    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        codeFile.seek( i*m_BlockSize +  m_CodeOffset);
        codeFile.write(code[i],m_WorkNum * CodeInfoManager::getBlockSize());
    }

    for(int i = 0 ; i < CodeInfoManager::getK() ; ++i){
        free(data[i]);
    }
    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        free(code[i]);
    }

    NodeReply* reply = new NodeReply(m_FileID,m_WorkNum);
    emit finished(reply);
}
