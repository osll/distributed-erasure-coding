#include "decodersc.h"

#include <QFile>
#include <QDebug>
#include <QVector>
#include <algorithm>

#include <General/codeinfomanager.h>
#include <Network/decodetaskdata.h>

#include <jerasure.h>
#include <jerasure/reed_sol.h>

DecodeRSC::DecodeRSC(qint32  fileID,
                     QString dataPath, qint32 dataOffset,
                     QString codePath, qint32 codeOffset,
                     QList<qint32> erasures,
                     qint32  blockSize       , qint32  workNum)
    :m_Erasures(erasures)
{
    m_FileID = fileID;
    m_DataPath = dataPath;
    m_CodePath = codePath;
    m_DataOffset = dataOffset;
    m_CodeOffset = codeOffset;
    m_Erasures = erasures;
    m_BlockSize = blockSize;
    m_WorkNum = workNum;

}

DecodeRSC::~DecodeRSC(){

}


void DecodeRSC::run(){
    QVector<int>   erasures(m_Erasures.length(),-1);

    qDebug() << m_DataOffset<< " "<< m_CodeOffset<< " " << m_Erasures;
    int i = 0;
    for(QList<int>::iterator it = m_Erasures.begin() ; it != m_Erasures.end(); ++it,++i){
        erasures[i] = *it;
    }
    Q_ASSERT(erasures[m_Erasures.length() - 1] == -1);

    QFile dataFile(m_DataPath);
    QFile codeFile(m_CodePath);

    if(!dataFile.open(QIODevice::ReadWrite))return;
    if(!codeFile.open(QIODevice::ReadWrite))return;

    QVector<char*> data(CodeInfoManager::getK(),NULL);
    QVector<char*> code(CodeInfoManager::getM(),NULL);

    for(int i = 0 ; i < CodeInfoManager::getK() ; ++i){
        data[i] =(char*) malloc(m_WorkNum * CodeInfoManager::getBlockSize());
        QVector<int>::iterator it = std::find(erasures.begin(),erasures.end(),i);
        if( it == erasures.end()){
            dataFile.seek( i*m_BlockSize +  m_DataOffset);
            dataFile.read(data[i],m_WorkNum * CodeInfoManager::getBlockSize());
        }
    }
    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        code[i] =(char*) malloc(m_WorkNum * CodeInfoManager::getBlockSize());
        QVector<int>::iterator it = std::find(erasures.begin(),erasures.end(),i + CodeInfoManager::getK());
        if( it == erasures.end()){
            codeFile.seek( i*m_BlockSize +  m_DataOffset);
            codeFile.read(code[i],m_WorkNum * CodeInfoManager::getBlockSize());
        }
    }

    qDebug() << "Кодирую :";
    jerasure_matrix_decode(CodeInfoManager::getK(), CodeInfoManager::getM(), CodeInfoManager::getW(),
                           CodeInfoManager::getMatrix(), 1, erasures.data(), data.data(), code.data(),
                           m_WorkNum * CodeInfoManager::getBlockSize());
    qDebug() << "Кодироваине завершено";

    if(!dataFile.exists())return;
    if(!codeFile.exists())return;

    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        codeFile.seek( i*m_BlockSize +  m_CodeOffset);
        codeFile.write(code[i],m_WorkNum * CodeInfoManager::getBlockSize());
    }

    for( QVector<int>::iterator it = erasures.begin(); it != erasures.end() ; ++it){
        if(*it == -1) continue;
        if(*it < CodeInfoManager::getK()){
            dataFile.seek( (*it)*m_BlockSize +  m_DataOffset);
            dataFile.write(data[(*it)],m_WorkNum * CodeInfoManager::getBlockSize());

        }else{
            codeFile.seek( (*it - CodeInfoManager::getK())*m_BlockSize +  m_CodeOffset);
            codeFile.write(code[(*it - CodeInfoManager::getK())],m_WorkNum * CodeInfoManager::getBlockSize());
        }
    }
    for(int i = 0 ; i < CodeInfoManager::getK() ; ++i){
        free(data[i]);
    }
    for(int i = 0 ; i < CodeInfoManager::getM() ; ++i){
        free(code[i]);
    }

    dataFile.close();
    codeFile.close();

    NodeReply* reply = new NodeReply(m_FileID,m_WorkNum);
    emit finished(reply);
}
