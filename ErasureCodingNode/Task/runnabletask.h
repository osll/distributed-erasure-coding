#ifndef RUNNABLETASK_H
#define RUNNABLETASK_H

#include <QObject>
#include <QRunnable>
#include <Network/nodereply.h>

class RunnableTask : public QObject,public QRunnable
{
    Q_OBJECT
public:
    RunnableTask();
    ~RunnableTask() = 0;

signals:
    void finished(NodeReply*);

public slots:

};

#endif // RUNNABLETASK_H
