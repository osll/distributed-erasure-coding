SOURCES += \
    $$PWD/encodersc.cpp \
    $$PWD/decodersc.cpp \
    $$PWD/taskfactory.cpp \
    $$PWD/rsvtaskfactory.cpp \
    $$PWD/runnabletask.cpp

HEADERS += \
    $$PWD/encodersc.h \
    $$PWD/decodersc.h \
    $$PWD/taskfactory.h \
    $$PWD/rsvtaskfactory.h \
    $$PWD/runnabletask.h
