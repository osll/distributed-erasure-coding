#include "console.h"

#include <QCoreApplication>
#include <unistd.h>


Console::Console(QObject *parent) :
    QObject(parent),m_Notifier(STDIN_FILENO, QSocketNotifier::Read)
{
    connect(&m_Notifier, SIGNAL(activated(int)), this, SLOT(proceedCommand()));
}

Console::~Console(){

}

void Console::proceedCommand()
{
    QTextStream in(stdin);
    QString line = in.readLine();
    if(line.trimmed() == "quit"){
        emit finished();
        QCoreApplication::quit();
    }else{
        qDebug()<<"commmand: "<<line;
    }
}
