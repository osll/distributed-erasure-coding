#ifndef CORE_H
#define CORE_H

#include <QDebug>
#include <QObject>
#include <QTextStream>
#include <QSocketNotifier>

class Console : public QObject
{
    Q_OBJECT
private:
    QSocketNotifier m_Notifier;

public:
    explicit Console(QObject *parent = 0);
    ~Console();

signals:
    void textReceived(QString command);
    void finished();

public slots:
    void proceedCommand();
};

#endif // CORE_H
