#-------------------------------------------------
#
# Project created by QtCreator 2014-11-15T17:11:19
#
#-------------------------------------------------
include(Task/Task.pri)

QT       += core
QT       += network

QT       -= gui

TARGET = ErasureCodingNode
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    servermanager.cpp \
    taskmanager.cpp \
    node.cpp \
    console.cpp

HEADERS += \
    servermanager.h \
    taskmanager.h \
    node.h \
    console.h

unix: LIBS += -L$$OUT_PWD/../TaskModule/ -lTaskModule

INCLUDEPATH += $$PWD/../TaskModule
DEPENDPATH += $$PWD/../TaskModule

unix: PRE_TARGETDEPS += $$OUT_PWD/../TaskModule/libTaskModule.a
##

unix: LIBS += -L$$PWD/../../../../../../../../../usr/local/lib/ -lJerasure

INCLUDEPATH += $$PWD/../../../../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../../../../usr/local/include
