#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

#include <Network/taskpacket.h>
#include <Network/nodereply.h>
#include <General/codeinfo.h>

class ServerManager : public QObject
{
    typedef QList<TaskPacket*>::iterator task_iterator;
    typedef QList<NodeReply*>::iterator node_iterator;

    Q_OBJECT
private:
    QTcpServer* m_pServer;
    QString     m_ipv4adress;
    int         m_Port;

    QTcpSocket* m_pSocket;

public:
    ServerManager(QString adress = "127.0.0.1", int port = 23473, QObject *parent = 0);
    ~ServerManager();

signals:
    void newTaskReady(TaskPacket*);
    void newDataFromServer(int numberOfThreads);
    void disconnectedFromHost();

public slots:
    void newConnection();
    void initDataReady();
    void readyRead();
    void acceptErrorhandler(QAbstractSocket::SocketError);
    void sendReply(NodeReply* reply);
    void reconnect();

};

#endif // SERVERMANAGER_H
