#include "servermanager.h"

#include <QHostAddress>
#include <QThread>

#include <General/codeinfomanager.h>

ServerManager::ServerManager(QString adress, int port, QObject *parent) :
    QObject(parent)
{
    m_ipv4adress = adress;
    m_Port = port;
    m_pServer = new QTcpServer();
    m_pSocket = NULL;
    qDebug() << "Starting QTcpServer";
    if(!m_pServer->listen(QHostAddress(m_ipv4adress), m_Port)){
        qDebug() << "Could not start QTcpServer";
        qDebug() << m_pServer->errorString();
        throw;
    }
    qDebug() << "Done Starting QTcpServer";
    connect(m_pServer, SIGNAL(newConnection()), this, SLOT(newConnection()));
    connect(m_pServer, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(acceptErrorhandler(QAbstractSocket::SocketError)));
}

void ServerManager::newConnection(){
    qDebug()<<"Server connected to me";
    m_pSocket = m_pServer->nextPendingConnection();

    connect(m_pSocket,SIGNAL(readyRead()),this,SLOT(initDataReady()));
    connect(m_pSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(acceptErrorhandler(QAbstractSocket::SocketError)));
    connect(m_pSocket,SIGNAL(disconnected()),this,SLOT(reconnect()));
}

void ServerManager::initDataReady(){
    qDebug()<<"Initializing from server message";
    disconnect(m_pSocket,SIGNAL(readyRead()),this,SLOT(initDataReady()));
    int numberOfProcessors;
    int K,M,W;
    qint32 type;

    qint32 size = 0;
    QDataStream in(m_pSocket);
    in.setVersion(QDataStream::Qt_5_3);

    while(true) {
        if (size == 0) {
            if (m_pSocket->bytesAvailable() < sizeof(qint32)) { // are size data available
                continue;
            }
            in >> size;
        }

        if (m_pSocket->bytesAvailable() < size) {
            continue;
        }
        in >> type;
        in >> K;
        in >> M;
        in >> W;
        in >> numberOfProcessors;
        qDebug() << "type" << type;
        qDebug() << "K" << K;
        qDebug() << "M" << M;
        qDebug() << "W" << W;
        qDebug() << "numberOfProcessors" << numberOfProcessors;
        break;
    }
    CodeInfoManager::setRSVM(K,M,W);
    emit newDataFromServer(numberOfProcessors);
    connect(m_pSocket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    qDebug()<<"Done Initializing from server message";
}

void ServerManager::reconnect(){
    qDebug()<<"Server disconnected from me";
    disconnect(m_pSocket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(m_pSocket,SIGNAL(readyRead()),this,SLOT(initDataReady()));
    emit disconnectedFromHost();
}

ServerManager::~ServerManager(){
    if(m_pSocket)m_pSocket->close();
    m_pServer->close();
    delete m_pServer;
}

void ServerManager::readyRead(){
    qDebug() << " Задание прислали";
    qint32 size = 0;
    QDataStream in(m_pSocket);
    in.setVersion(QDataStream::Qt_5_3);

    while(true) {
        if (size == 0) {
            if (m_pSocket->bytesAvailable() < sizeof(qint32)) { // are size data available
                continue;
            }
            in >> size;
        }

        if (m_pSocket->bytesAvailable() < size) {
            continue;
        }
        TaskPacket* task = new TaskPacket();
        in >> *task;
        emit newTaskReady(task);

        if(m_pSocket->bytesAvailable()){
            size = 0;
        }else break;
    }
}

void ServerManager::acceptErrorhandler(QAbstractSocket::SocketError socketError){
    switch(socketError){
        //TODO написать обработчики ошибок
    }
    qDebug() << "QTcpServer error: " << m_pSocket->errorString();
}

void ServerManager::sendReply(NodeReply* reply){
    if(m_pSocket->isOpen()){
        QByteArray block;
        QDataStream out(&block,QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_3);

        out << (qint32)0;
        out << *reply;
        out.device()->seek(0);
        out << (qint32)(block.size() - sizeof(qint32));

        m_pSocket->write(block);
        qDebug()<<"Successfully sent reply. FileID:'"<< reply->getFileID() << "'' Done workd:'"<<reply->getDoneWorkNum() << "'";
    }else{
        qDebug()<<"Discarded NodeRaply becouse host disconnected";
    }
    delete reply;
}


