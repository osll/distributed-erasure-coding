#include "taskmanager.h"

#include <QThreadPool>

#include <General/codeinfomanager.h>
#include <Network/tasktype.h>

#include "Task/rsvtaskfactory.h"


TaskManager::TaskManager(int numberOfThreads,QObject *parent) :
    QObject(parent)
{
    switch (CodeInfoManager::getMethodType()) {
    case ReedSolomonVandermondeMatrix:
        m_pTaskFactory = new RSVTaskFactory();
        break;
    case Undefined:
        Q_ASSERT(false);
    }
    QThreadPool::globalInstance()->setMaxThreadCount(numberOfThreads);
    QThreadPool::globalInstance()->setExpiryTimeout(-1);
}

TaskManager::~TaskManager(){
    QThreadPool::globalInstance()->clear();
    if(m_pTaskFactory)delete m_pTaskFactory;
    m_pTaskFactory = NULL;
}

void TaskManager::executeTask(TaskPacket* packet){
    RunnableTask* task;

    switch(packet->getOPcode()){
    case Encoding:
        task = m_pTaskFactory->createEncodeTask(packet);
        break;
    case Decoding:
        task = m_pTaskFactory->createDecodeTask(packet);
        break;
    }
    connect(task,SIGNAL(finished(NodeReply*)),this,SLOT(sendReply(NodeReply*)));
    QThreadPool::globalInstance()->start(task);
    delete packet;
}

void TaskManager::sendReply(NodeReply * reply){
    emit newReply(reply);
}

