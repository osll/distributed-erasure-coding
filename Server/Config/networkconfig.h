#ifndef NWORKCONFIG_H
#define NWORKCONFIG_H

#include <QObject>

class NetworkConfig;

class NetworkConfigDestroyer
{
private:
    NetworkConfig* p_instance;
public:
    ~NetworkConfigDestroyer();
    void initialize( NetworkConfig* p );
};

class NetworkConfig
{
private:
    int m_TotalNumberOfProcessors;
    int m_NumberOfNodes;
    QString m_NASPath;

    static NetworkConfig* p_instance;
    static NetworkConfigDestroyer destroyer;

protected:
    NetworkConfig();
    NetworkConfig( const NetworkConfig& );
    NetworkConfig& operator=( NetworkConfig& );
   ~NetworkConfig() { }
    friend class NetworkConfigDestroyer;

public:
    static NetworkConfig* getInstance();

    int getTotalNumberOfProcessors();
    int getNumberOfNodes();
    QString getNASPath();

    void setTotalNumberOfProcessors(int);
    void setNumberOfNodes(int);
    void setNASPath(QString);
};

#endif // NETWORKCONFIG_H
