#include "networkconfig.h"


NetworkConfig * NetworkConfig::p_instance = 0;
NetworkConfigDestroyer NetworkConfig::destroyer;

NetworkConfigDestroyer::~NetworkConfigDestroyer() {
    delete p_instance;
}
void NetworkConfigDestroyer::initialize( NetworkConfig* p ) {
    p_instance = p;
}

NetworkConfig* NetworkConfig::getInstance() {
    if(!p_instance)     {
        p_instance = new NetworkConfig();
        destroyer.initialize( p_instance);
    }
    return p_instance;
}


NetworkConfig::NetworkConfig()
{
    m_NumberOfNodes = 0;
    m_TotalNumberOfProcessors = 0;
    m_NASPath = "";
}

void NetworkConfig::setTotalNumberOfProcessors(int thread_num){
    m_TotalNumberOfProcessors = thread_num;
}

void NetworkConfig::setNumberOfNodes(int node_num){
    m_NumberOfNodes = node_num;
}

void NetworkConfig::setNASPath(QString path){
    m_NASPath = path;
}

int NetworkConfig::getTotalNumberOfProcessors(){
    return m_TotalNumberOfProcessors;
}

int NetworkConfig::getNumberOfNodes(){
    return m_NumberOfNodes;
}

QString NetworkConfig::getNASPath(){
    return m_NASPath;
}
