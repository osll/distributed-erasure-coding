#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QSettings>

#include <General/codeinfo.h>

#include "console.h"
#include "NodeManager/nodemanager.h"
#include "TaskManager/taskmanager.h"

class Server : public QObject
{
    Q_OBJECT
private:
    TaskManager* m_pTaskManager;
    NodeManager* m_pNodeManager;
    Console*     m_pConsole;

protected:
    bool parseNAS           (QSettings& stream);
    bool parseCodeInfo      (QSettings& stream);
    bool parseServerSettings(QSettings& stream);
    bool parseNode          (QSettings& stream);
    bool parseNodeList      (QSettings& stream);
    void parseSettings      (QSettings& stream);

public:
    explicit Server(QObject *parent = 0);

    void initialize();

signals:

public slots:
    void encodeFile(QString path);
    void decodeFile(QString path);

    void consoleFinished();

};

#endif // SERVER_H
