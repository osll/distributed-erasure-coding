#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include <QList>
#include <QStringList>

#include <Network/nodereply.h>
#include <Network/taskpacket.h>

#include "Config/networkconfig.h"
#include "taskinfo.h"


class TaskManager : public QObject
{
    Q_OBJECT

private:
    QList<TaskInfo*>* m_pTaskList;

    static int s_IDgenerator;

protected:
    static int getnewID();
    void addTask(TaskInfo* task);
    QList<TaskInfo*>* getTaskList();

public:
    TaskManager();
    virtual ~TaskManager() = 0;

    virtual int erasureEncoding(QString path) = 0;
    virtual int erasureDecoding(QString path) = 0;

signals:
    void newTasks(QList<TaskPacket*>*);

public slots:
    void newReplyReady(NodeReply*);

protected slots:
    virtual void taskFinished(TaskInfo*) = 0;

};

#endif // TASKMANAGER_H
