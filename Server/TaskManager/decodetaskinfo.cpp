#include "decodetaskinfo.h"

DecodeTaskInfo::DecodeTaskInfo(qint32 fileID,QString filename, qint32 size, qint32 allWork, QString dataPath, QString codePath)
    :TaskInfo(fileID, filename, size, allWork, dataPath, codePath)
{
    setType(Decoding);
    m_pErasures = new QList<qint32>();
    m_pErasures->push_front(-1);
}

void DecodeTaskInfo::addErasure(qint32 id){
    m_pErasures->push_front(id);
}

QList<qint32>* DecodeTaskInfo::getErasures() const{
    return m_pErasures;
}

DecodeTaskInfo::~DecodeTaskInfo(){
    delete m_pErasures;
}
