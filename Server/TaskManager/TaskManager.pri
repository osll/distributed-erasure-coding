INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


SOURCES += \
    $$PWD/taskmanager.cpp \
    $$PWD/taskinfo.cpp \
    $$PWD/decodetaskinfo.cpp \
    $$PWD/reedsolomonvandermondtaskmanager.cpp


HEADERS += \
    $$PWD/taskmanager.h \
    $$PWD/taskinfo.h \
    $$PWD/decodetaskinfo.h \
    $$PWD/reedsolomonvandermondtaskmanager.h
