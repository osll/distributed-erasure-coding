#ifndef REEDSOLOMONVANDERMONDTASKMANAGER_H
#define REEDSOLOMONVANDERMONDTASKMANAGER_H

#include "taskmanager.h"

class ReedSolomonVandermondTaskManager : public TaskManager
{
    Q_OBJECT
private:

public:
    ReedSolomonVandermondTaskManager();
    ~ReedSolomonVandermondTaskManager();

    int erasureEncoding(QString path);
    int erasureDecoding(QString path);

signals:

protected slots:
    void taskFinished(TaskInfo*);

};

#endif // REEDSOLOMONVANDERMONDTASKMANAGER_H
