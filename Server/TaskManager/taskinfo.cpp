#include "taskinfo.h"
#include <QDebug>

TaskInfo::TaskInfo(qint32 fileID,QString filename, qint32 size, qint32 allWork,
                   QString dataPath, QString codePath)
    :QObject(0)
{
    setType(Encoding);
    setFileID(fileID);
    setFilename(filename);
    setSize(size);
    setDoneWork(0);
    setAllWork(allWork);

    setDataPath(dataPath);
    setCodePath(codePath);
}

TaskInfo::~TaskInfo(){
}

void TaskInfo::setFileID  (qint32 fileID){
    m_FIleID = fileID;
}
void TaskInfo::setFilename(QString filename){
    m_Filename = filename;
}
void TaskInfo::setSize(qint32 size){
    m_Size = size;
}
void TaskInfo::setDoneWork(qint32 doneWork){
    m_DoneWork = doneWork;
}
void TaskInfo::setAllWork (qint32 allWork){
    m_AllWork = allWork;
}
void TaskInfo::setDataPath(QString path){
    m_DataPath = path;
}
void TaskInfo::setCodePath(QString path){
    m_CodePath = path;
}
void TaskInfo::setType(TaskType type){
    m_Type = type;
}


qint32 TaskInfo::getFileID(){
    return m_FIleID;
}
QString TaskInfo::getFilename(){
    return m_Filename;
}
qint32 TaskInfo::getSize(){
    return m_Size;
}
qint32 TaskInfo::getDoneWork(){
    return m_DoneWork;
}
qint32 TaskInfo::getAllWork(){
    return m_AllWork;
}
QString TaskInfo::getDataPath(){
    return m_DataPath;
}
QString TaskInfo::getCodePath(){
    return m_CodePath;
}
TaskType TaskInfo::getType(){
    return m_Type;
}

void TaskInfo::addDoneWork(qint32 doneWork){
    setDoneWork(getDoneWork() + doneWork);
    qDebug()<<"FileID " << getFileID() <<"|Done " << getDoneWork() << "|To be done "<<m_AllWork-getDoneWork();
    if(isDone())emit taskFinished(this);
}
bool TaskInfo::isDone(){
    Q_ASSERT(m_DoneWork <= m_AllWork);
    return m_DoneWork == m_AllWork;
}
