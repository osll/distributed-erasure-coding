#include "server.h"

#include <QDir>
#include <QFile>
#include <QCoreApplication>

#include <General/codeinfomanager.h>
#include "reedsolomonvandermondtaskmanager.h"


Server::Server(QObject *parent) :
    QObject(parent)
{
    m_pNodeManager = new NodeManager();
    initialize();
    m_pConsole = new Console();

    connect(m_pConsole,SIGNAL(encode(QString)),this,SLOT(encodeFile(QString)));
    connect(m_pConsole,SIGNAL(decode(QString)),this,SLOT(decodeFile(QString)));
}

void Server::consoleFinished(){
    delete m_pConsole;
    m_pConsole = NULL;
}


bool Server::parseNAS(QSettings& settings){
    QString sfpath = "";
    sfpath = settings.value("SharedFolderPath").toString();
    Q_ASSERT(sfpath != "");
    NetworkConfig::getInstance()->setNASPath(sfpath);

    QDir codingDir = QDir(sfpath);

    if(codingDir.cd(tr("Data"))){
        if(!codingDir.removeRecursively()) Q_ASSERT(false);
    }
    codingDir.cdUp();
    if(codingDir.cd(tr("Finished"))){
        if(!codingDir.removeRecursively()) Q_ASSERT(false);
    }
    codingDir.cdUp();

    if( !codingDir.mkdir(tr("Data")) ) Q_ASSERT(false);
    if( !codingDir.mkdir(tr("Finished")) ) Q_ASSERT(false);
    codingDir.cd("Finished");
    if( !codingDir.mkdir(tr("Encode")) ) Q_ASSERT(false);
    if( !codingDir.mkdir(tr("Decode")) ) Q_ASSERT(false);

    return true;
}

bool Server::parseNode(QSettings& settings){
    QString name, ipv4Adress = "";
    int port = -1;
    int numberOfProcessors = -1;

    name = settings.value("Name").toString();
    ipv4Adress = settings.value("ipv4Adress").toString();
    port = settings.value("tcpPort").toInt();
    numberOfProcessors = settings.value("numberOfProcessors").toInt();

    Q_ASSERT(ipv4Adress != "" && port > 0 && numberOfProcessors > 0 );
    m_pNodeManager->addNode(ipv4Adress,port,name,numberOfProcessors);
    NetworkConfig::getInstance()->setNumberOfNodes(NetworkConfig::getInstance()->getNumberOfNodes() + 1);
    NetworkConfig::getInstance()->setTotalNumberOfProcessors(NetworkConfig::getInstance()->getTotalNumberOfProcessors() + numberOfProcessors);
    return true;
}



bool Server::parseCodeInfo(QSettings& settings){
    QString methodName = settings.value("MethodName").toString();
    if(methodName == "ReedSolomonVandermondeMatrix"){
        int K = -1;
        int M = -1;
        int W = -1;

        K = settings.value("K").toInt();
        M = settings.value("M").toInt();
        W = settings.value("W").toInt();

        Q_ASSERT(K > 0 && M > 0 && W > 0);
        CodeInfoManager::setRSVM(K,M,W);
        m_pTaskManager = new ReedSolomonVandermondTaskManager();

        connect(m_pNodeManager,SIGNAL(newReplyReady(NodeReply*)),m_pTaskManager,SLOT(newReplyReady(NodeReply*)));
        connect(m_pTaskManager,SIGNAL(newTasks(QList<TaskPacket*>*)),m_pNodeManager,SLOT(sendTasks(QList<TaskPacket*>*)));
    }
    return true;
}


void Server::initialize(){

    QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,QCoreApplication::applicationDirPath());
    QSettings::setPath(QSettings::IniFormat,QSettings::SystemScope,QCoreApplication::applicationDirPath());

    QSettings settings(QCoreApplication::applicationDirPath() + "/settings",QSettings::IniFormat);

    settings.beginGroup("CodeInfo");
    parseCodeInfo(settings);
    settings.endGroup();

    settings.beginGroup("NAS");
    parseNAS(settings);
    settings.endGroup();

    int size = settings.beginReadArray("NodeArray");
    for(int i = 0 ; i < size ;++i){
        settings.setArrayIndex(i);
        parseNode(settings);
    }
    settings.endArray();
}


void Server::encodeFile(QString path){
    m_pTaskManager->erasureEncoding(path);
}


void Server::decodeFile(QString path){
    m_pTaskManager->erasureDecoding(path);
}
