#include "nodemanager.h"
#include <QDebug>


NodeManager::NodeManager()
{
    m_pNodeList = new QList<NodeInfo*>();
}

NodeManager::~NodeManager(){
    for(node_iterator it = begin(); it != end(); ++it){
        delete *it;
    }
    delete m_pNodeList;
}

void NodeManager::readyRead(NodeReply* reply){
    emit newReplyReady(reply);
}

NodeManager::node_iterator NodeManager::begin(){
    return m_pNodeList->begin();
}

NodeManager::node_iterator NodeManager::end(){
    return m_pNodeList->end();
}

void NodeManager::addNode(QString adress,int port, QString name,qint32 numberOfProcessors){
    NodeInfo* p_node= new NodeInfo(adress, port, name, numberOfProcessors);
    m_pNodeList->push_back(p_node);
    connect(p_node,SIGNAL(readyRead(NodeReply*)),this,SLOT(readyRead(NodeReply*)));
}

void NodeManager::sendTasks(QList<TaskPacket*>* tastList){
    task_iterator it_task =  tastList->begin();
    while(it_task != tastList->end())
    {
        for(node_iterator it_node = m_pNodeList->begin(); it_node != m_pNodeList->end() ; ++it_node,++it_task){
            if(it_task == tastList->end()) break;
            (*it_node)->sendTask(**it_task);
            delete *it_task;
        }
    }
}

