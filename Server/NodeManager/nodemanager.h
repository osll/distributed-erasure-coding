#ifndef NODEMANAGER_H
#define NODEMANAGER_H

#include <QObject>
#include <QList>

#include <Network/nodereply.h>
#include <Network/taskpacket.h>

#include "nodeinfo.h"


class NodeManager : public QObject
{
    Q_OBJECT

    typedef  QList<NodeInfo*>::iterator node_iterator;
    typedef  QList<TaskPacket*>::iterator task_iterator;

private:
    QList<NodeInfo*>*  m_pNodeList;

protected:
    node_iterator begin();
    node_iterator end();

public:
    explicit NodeManager();
    ~NodeManager();

    void addNode(QString adress,int port, QString name, qint32 numberOfProcessors);

signals:
    void newReplyReady(NodeReply*);
    void sendTaskError();

private slots:
    void readyRead(NodeReply*);
    void sendTasks(QList<TaskPacket*>* tastList);

};

#endif // NODEMANAGER_H
